---
layout: handbook-page-toc
title: 5 Minute Production App Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## 5 Minute Production App Single-Engineer Group

The 5 Minute Production App is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

The 5 Minute Production App group aims to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329595](https://gitlab.com/gitlab-org/gitlab/-/issues/329595)

## Reading list and further resources

* [https://www.youtube.com/watch?v=XJ-mmu25mdg](Secure Deployments from GitLab to Google App Engine)
* [https://docs.google.com/document/d/1xp0Ax5_svn8uwB75pM_4tDp4fHcPHB6pSFDWWUym2a4/edit#heading=h.uw7kque3pc2p](5 minute production app (5MPA): Auto Devops Production app with Terraform and Amazon EC2, RDS, S3)